# Qantas Money Android

## MVP

MVP architecture is chosen because in my opinion it has a very clear separation of responsibilities between components, such as UI and business rules

## Coroutines

For what the project needs, Coroutines makes perfect sense to be used to make asynchronous call, for example for making service calls

## Koin

Koin is chosen for Dependency Injection framework over Dagger because it's easier to set up for such a small project with limited time

## Networking Libraries

Retrofit and OkHttp are used because they makes networking easy, quick, and painless

## UI

* The UI are simple and basic with most of the fonts are the same throughout the app
* For part 2 of the task, the transactions is ordered from newest to oldest
* For part 3, map is displayed where the merchant address contains valid latitude and longitude

## With more time ...

* UI can be much better with screen transition
* More tests including UI test
