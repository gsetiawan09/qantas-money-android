package com.q.gunadi.transactions.model

class TransactionItem(val transaction: Transaction) : TransactionListItem() {
    override val type: Int
        get() = TYPE_TRANSACTION
}
