package com.q.gunadi.transactions.view.ui

import com.q.gunadi.BaseView
import com.q.gunadi.transactions.model.TransactionListItem
import com.q.gunadi.transactions.model.Transaction
import com.q.gunadi.transactions.view.adapter.TransactionListAdapter
import com.q.gunadi.transactions.presenter.TransactionsPresenter

interface TransactionsView : BaseView<TransactionsPresenter> {
    var transactionListAdapter: TransactionListAdapter?
    fun displayTransactions(transactions: List<TransactionListItem>)
    fun showTransactionDetails(transaction: Transaction)
}
