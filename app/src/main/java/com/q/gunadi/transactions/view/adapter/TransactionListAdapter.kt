package com.q.gunadi.transactions.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.q.gunadi.utils.OnSingleClickListener
import com.q.gunadi.R
import com.q.gunadi.transactions.model.DateItem
import com.q.gunadi.transactions.model.TransactionListItem
import com.q.gunadi.transactions.model.Transaction
import com.q.gunadi.transactions.model.TransactionItem
import com.q.gunadi.utils.getAmountText
import kotlinx.android.synthetic.main.view_item_date.view.*
import kotlinx.android.synthetic.main.view_item_transaction.view.*

class TransactionListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()  {

    interface ActionListener {
        fun onTransactionItemClicked(transaction: Transaction)
    }

    var data: List<TransactionListItem> = arrayListOf()

    var listener: ActionListener? = null

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(description: String,
                 amount: Number,
                 currency: String,
                 merchantName: String,
                 isPending: Boolean,
                 clickListener: View.OnClickListener) {

            itemView.description.text = description
            itemView.balance.text =
                getAmountText(currency, amount)
            itemView.available_balance.text = merchantName
            itemView.pending.visibility = if (isPending) View.VISIBLE else View.GONE
            itemView.setOnClickListener(clickListener)
        }
    }

    class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(date: String) {
            itemView.date.text = date
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TransactionListItem.TYPE_TRANSACTION -> TransactionViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.view_item_transaction, parent, false
                )
            )
            else -> DateViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.view_item_date, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TransactionViewHolder -> {
                (data[position] as? TransactionItem)?.transaction?.let { transaction ->
                    holder.bind(transaction.description,
                        transaction.amount,
                        transaction.currency,
                        transaction.merchant.name,
                        transaction.isPending,
                        OnSingleClickListener {
                            listener?.onTransactionItemClicked(
                                transaction
                            )
                        })
                }
            }
            else -> {
                (holder as? DateViewHolder)?.bind((data[position] as DateItem).date)
            }
        }
    }

    override fun getItemCount() = data.size

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is TransactionItem -> TransactionListItem.TYPE_TRANSACTION
            else -> TransactionListItem.TYPE_DATE
        }
    }
}
