package com.q.gunadi.transactions.model

class DateItem(val date: String) : TransactionListItem() {
    override val type: Int
        get() = TYPE_DATE
}
