package com.q.gunadi.transactions.view.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.q.gunadi.BaseActivity
import com.q.gunadi.R
import com.q.gunadi.databinding.ActivityTransactionsBinding
import com.q.gunadi.transactiondetails.view.TransactionDetailsActivity
import com.q.gunadi.transactions.model.TransactionListItem
import com.q.gunadi.transactions.model.Transaction
import com.q.gunadi.transactions.presenter.TransactionsPresenter
import com.q.gunadi.transactions.view.adapter.TransactionListAdapter
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class TransactionsActivity : BaseActivity<TransactionsView, TransactionsPresenter>(),
    TransactionsView {

    companion object {
        @JvmStatic
        fun createIntent(context: Context) = Intent(context, TransactionsActivity::class.java)
    }

    override val presenter: TransactionsPresenter by inject { parametersOf(this) }

    private lateinit var binding: ActivityTransactionsBinding

    override var transactionListAdapter: TransactionListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransactionsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = getString(R.string.transactions_title)
        }

        transactionListAdapter =
            TransactionListAdapter()
        binding.accountsList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@TransactionsActivity)
            adapter = transactionListAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        presenter.onViewCreated()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun displayTransactions(transactions: List<TransactionListItem>) {
        Timber.d("displayTransactions")
        transactionListAdapter?.let {
            it.data = transactions
            it.notifyDataSetChanged()
        }
    }

    override fun displayProgress() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.progressBar.visibility = View.GONE
    }

    override fun showTransactionDetails(transaction: Transaction) {
        startActivity(TransactionDetailsActivity.createIntent(this, transaction))
    }
}
