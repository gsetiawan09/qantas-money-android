package com.q.gunadi.transactions.presenter

import com.q.gunadi.BasePresenter
import com.q.gunadi.transactions.model.*
import com.q.gunadi.transactions.view.ui.TransactionsView
import com.q.gunadi.transactions.view.adapter.TransactionListAdapter
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import org.koin.core.inject

class TransactionsPresenter(
    private val view: TransactionsView
) : BasePresenter<TransactionsView>(),
    TransactionListAdapter.ActionListener, KoinComponent {

    private val transactionsModel: TransactionsModel by inject()

    fun onViewCreated() {
        view.transactionListAdapter?.listener = this
        loadTransactions()
    }

    internal fun loadTransactions() {
        view.displayProgress()
        launch {
            try {
                val transactions = transactionsModel.getTransactions()
                withContext(Dispatchers.Main) {
                    view.hideProgress()
                    view.displayTransactions(transactions)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    view.hideProgress()
                    view.displayError(e.toString())
                }
            }
        }
    }

    override fun onTransactionItemClicked(transaction: Transaction) {
        view.showTransactionDetails(transaction)
    }
}
