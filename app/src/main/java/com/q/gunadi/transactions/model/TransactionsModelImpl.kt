package com.q.gunadi.transactions.model

import com.q.gunadi.APIService
import com.q.gunadi.utils.getDateOnly
import org.koin.core.KoinComponent
import org.koin.core.inject

class TransactionsModelImpl : TransactionsModel, KoinComponent {

    private val apiService: APIService by inject()

    override suspend fun getTransactions() = populateTransactionListItems(apiService.fetchTransactions().transactions)

    private fun populateTransactionListItems(transactions: List<Transaction>) : List<TransactionListItem> {
        val consolidatedList = arrayListOf<TransactionListItem>()

        val groupedTransactions = groupTransactionsByDate(transactions)
        groupedTransactions.forEach {
            consolidatedList.add(DateItem(it.key))
            it.value.forEach { transaction ->
                consolidatedList.add(
                    TransactionItem(
                        transaction
                    )
                )
            }
        }
        return consolidatedList
    }

    private fun groupTransactionsByDate(transactions: List<Transaction>): Map<String, MutableList<Transaction>> {
        val groupedTransactions: HashMap<String, MutableList<Transaction>> = HashMap()

        transactions.forEach { transaction ->
            val hashMapKey: String =
                getDateOnly(transaction.date)

            groupedTransactions[hashMapKey]?.add(transaction)
                ?: run {
                    val list: MutableList<Transaction> = ArrayList()
                    list.add(transaction)
                    groupedTransactions[hashMapKey] = list
                }
        }
        return groupedTransactions.toSortedMap((compareByDescending { it }))
    }
}
