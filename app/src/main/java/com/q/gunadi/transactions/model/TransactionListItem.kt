package com.q.gunadi.transactions.model

abstract class TransactionListItem {
    abstract val type: Int

    companion object {
        const val TYPE_DATE = 0
        const val TYPE_TRANSACTION = 1
    }
}
