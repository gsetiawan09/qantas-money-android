package com.q.gunadi.transactions.model

interface TransactionsModel {
    suspend fun getTransactions(): List<TransactionListItem>
}
