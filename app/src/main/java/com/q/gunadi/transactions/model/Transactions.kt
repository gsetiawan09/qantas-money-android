package com.q.gunadi.transactions.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Transactions(
    val transactions: List<Transaction>
) : Parcelable

@Parcelize
data class Transaction(
    val id: Number,
    @SerializedName("created") val date: String,
    val description: String,
    val amount: Number,
    val currency: String,
    val merchant: Merchant,
    @SerializedName("amount_is_pending") val isPending: Boolean
) : Parcelable

@Parcelize
data class Merchant(
    val name: String,
    val address: Address?,
    val updated: String?
) : Parcelable

@Parcelize
data class Address(
    @SerializedName("address") val street: String,
    val city: String?,
    val region: String?,
    val country: String?,
    val postcode: String?,
    val latitude: Number?,
    val longitude: Number?
) : Parcelable
