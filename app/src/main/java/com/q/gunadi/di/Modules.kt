package com.q.gunadi.di

import com.q.gunadi.APIService
import com.q.gunadi.BuildConfig
import com.q.gunadi.accounts.model.AccountsModel
import com.q.gunadi.accounts.model.AccountsModelImpl
import com.q.gunadi.accounts.presenter.AccountsPresenter
import com.q.gunadi.accounts.view.ui.AccountsView
import com.q.gunadi.transactiondetails.presenter.TransactionDetailsPresenter
import com.q.gunadi.transactiondetails.view.TransactionDetailsView
import com.q.gunadi.transactions.model.TransactionsModel
import com.q.gunadi.transactions.model.TransactionsModelImpl
import com.q.gunadi.transactions.presenter.TransactionsPresenter
import com.q.gunadi.transactions.view.ui.TransactionsView
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

private const val CONNECT_TIMEOUT = 15L
private const val READ_TIMEOUT = 15L
private const val WRITE_TIMEOUT = 15L
private const val BASE_URL = "https://jsonstorage.net/api/items/"
private const val OK_HTTP_LOGGING_TAG = "OkHttp"

val applicationModule = module(override = true) {
    factory { (view: AccountsView) ->
        AccountsPresenter(
            view
        )
    }
    single<AccountsModel> { AccountsModelImpl() }
    factory { (view: TransactionsView) ->
        TransactionsPresenter(
            view
        )
    }
    single<TransactionsModel> { TransactionsModelImpl() }
    factory { (view: TransactionDetailsView) ->
        TransactionDetailsPresenter(
            view
        )
    }
    single { retrofit(get()).create(APIService::class.java) }
    single { okHttp() }
}

fun okHttp(): OkHttpClient {
    val okHttpClientBuilder = OkHttpClient().newBuilder()
        .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)

    if (BuildConfig.DEBUG) {
        val httpLoggingInterceptor =
            HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Timber.tag(OK_HTTP_LOGGING_TAG).v(message)
                }
            })
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
    }

    return okHttpClientBuilder.build()
}

fun retrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(BASE_URL)
    .build()
