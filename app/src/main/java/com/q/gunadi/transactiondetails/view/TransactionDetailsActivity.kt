package com.q.gunadi.transactiondetails.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.q.gunadi.*
import com.q.gunadi.databinding.ActivityTransactionDetailsBinding
import com.q.gunadi.transactiondetails.presenter.TransactionDetailsPresenter
import com.q.gunadi.transactions.model.Transaction
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

private const val TRANSACTION = "transaction"
private const val DEFAULT_ZOOM = 13f

class TransactionDetailsActivity : BaseActivity<TransactionDetailsView, TransactionDetailsPresenter>(),
    TransactionDetailsView, OnMapReadyCallback {

    companion object {
        @JvmStatic
        fun createIntent(context: Context, transaction: Transaction) =
            Intent(context, TransactionDetailsActivity::class.java).apply {
                putExtra(TRANSACTION, transaction)
            }
    }

    override val presenter: TransactionDetailsPresenter by inject { parametersOf(this) }

    private lateinit var binding: ActivityTransactionDetailsBinding
    private lateinit var mapFragment: SupportMapFragment
    private var transaction: Transaction? = null

    private var location: LatLng? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransactionDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = getString(R.string.transaction_details_title)
        }

        transaction = intent.getParcelableExtra(TRANSACTION)

        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.view?.visibility = View.GONE

        presenter.onViewCreated(transaction)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onMapReady(googleMap: GoogleMap) {
        location?.let {
            val marker = MarkerOptions().position(it).title(transaction?.merchant?.name)
                .snippet(transaction?.merchant?.address?.street)
            googleMap.addMarker(marker).showInfoWindow()
            googleMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    it,
                    DEFAULT_ZOOM
                )
            )
        }
    }

    override fun displayTransactionDetails(
        id: Number,
        date: String,
        isPending: Boolean,
        description: String,
        amount: String,
        merchantName: String,
        merchantAddress: String?
    ) {
        binding.apply {
            transactionId.text = getString(R.string.transaction_id_field, id)
            transactionDate.text = date
            pending.visibility = if (isPending) View.VISIBLE else View.GONE
            this.description.text = description
            this.amount.text = amount
            this.merchantName.text = merchantName
        }

        merchantAddress?.let { address ->
            binding.merchantAddress.visibility = View.VISIBLE
            binding.merchantAddress.text = address
        }
    }

    override fun displayMap(latitude: Number, longitude: Number) {
        location = LatLng(
            latitude.toDouble(),
            longitude.toDouble()
        )
        mapFragment.view?.visibility = View.VISIBLE
        mapFragment.getMapAsync(this)
    }

    override fun displayProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
