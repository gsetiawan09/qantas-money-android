package com.q.gunadi.transactiondetails.presenter

import com.q.gunadi.BasePresenter
import com.q.gunadi.transactiondetails.view.TransactionDetailsView
import com.q.gunadi.utils.addNewField
import com.q.gunadi.utils.getAmountText
import com.q.gunadi.utils.getDateAndTime
import com.q.gunadi.transactions.model.Address
import com.q.gunadi.transactions.model.Transaction

class TransactionDetailsPresenter(
    private val view: TransactionDetailsView
) : BasePresenter<TransactionDetailsView>() {

    fun onViewCreated(transaction: Transaction?) {
        loadTransactionDetails(transaction)
    }

    internal fun loadTransactionDetails(transaction: Transaction?) {
        if (transaction == null) {
            view.displayError()
            return
        }

        view.displayTransactionDetails(
            transaction.id,
            getDateAndTime(transaction.date),
            transaction.isPending,
            transaction.description,
            getAmountText(
                transaction.currency,
                transaction.amount
            ),
            transaction.merchant.name,
            getFormattedAddress(transaction.merchant.address)
        )

        transaction.merchant.address?.let {
            if (it.latitude != null && it.longitude != null) {
                view.displayMap(it.latitude, it.longitude)
            }
        }
    }

    private fun getFormattedAddress(address: Address?): String? {
        return address?.let {
            ""
                .addNewField(it.street)
                .addNewField(it.city)
                .addNewField(it.region)
                .addNewField(it.postcode)
                .addNewField(it.country)
        }
    }
}
