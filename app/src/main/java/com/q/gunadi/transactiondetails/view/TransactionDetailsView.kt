package com.q.gunadi.transactiondetails.view

import com.q.gunadi.BaseView
import com.q.gunadi.transactiondetails.presenter.TransactionDetailsPresenter

interface TransactionDetailsView : BaseView<TransactionDetailsPresenter> {

    fun displayTransactionDetails(id: Number, date: String, isPending: Boolean, description: String, amount: String, merchantName: String, merchantAddress: String?)

    fun displayMap(latitude: Number, longitude: Number)

}
