package com.q.gunadi.utils

fun String.addNewField(field: String?) : String {
    return when {
        field.isNullOrEmpty() -> this
        this.isEmpty() -> this + field
        else -> this + "\n" + field
    }
}
