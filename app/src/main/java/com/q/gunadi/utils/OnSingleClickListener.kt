package com.q.gunadi.utils

import android.os.SystemClock
import android.view.View

private const val DEFAULT_INTERVAL = 1000

class OnSingleClickListener(private var interval: Int = DEFAULT_INTERVAL,
                            private val onSingleClick: (View) -> Unit) : View.OnClickListener {

    private var lastClickedTime: Long = 0

    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - lastClickedTime < interval) {
            return
        }
        lastClickedTime = SystemClock.elapsedRealtime()
        onSingleClick(v)
    }
}

fun View.setOnSingleClickListener(onSingleClick: (View) -> Unit) {
    setOnClickListener(OnSingleClickListener {
        onSingleClick(it)
    })
}
