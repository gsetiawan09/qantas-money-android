package com.q.gunadi.utils

import java.text.NumberFormat
import java.util.*

fun getAmountText(currency: String, amount: Number): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    format.currency = Currency.getInstance(currency)
    return format.format(amount)
}
