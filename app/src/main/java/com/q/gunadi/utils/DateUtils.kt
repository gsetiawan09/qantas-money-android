package com.q.gunadi.utils

import java.text.SimpleDateFormat
import java.util.*

private const val FULL_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
private const val DATE_ONLY_FORMAT = "dd MMMM yyyy"
private const val DATE_TIME_FORMAT = "dd MMMM yyyy hh:mm a"

fun getDateOnly(dateAndTime: String) =
    transform(
        SimpleDateFormat(
            FULL_DATE_FORMAT,
            Locale.getDefault()
        ),
        SimpleDateFormat(
            DATE_ONLY_FORMAT,
            Locale.getDefault()
        ),
        dateAndTime
    )

fun getDateAndTime(dateAndTime: String) =
    transform(
        SimpleDateFormat(
            FULL_DATE_FORMAT,
            Locale.getDefault()
        ),
        SimpleDateFormat(
            DATE_TIME_FORMAT,
            Locale.getDefault()
        ),
        dateAndTime
    )

fun transform(fromFormat: SimpleDateFormat, toFormat: SimpleDateFormat, dateAndTime: String) =
    fromFormat.parse(dateAndTime)?.let {
        toFormat.format(it)
    } ?: ""
