package com.q.gunadi

import com.q.gunadi.accounts.model.Accounts
import com.q.gunadi.transactions.model.Transactions
import retrofit2.http.GET

interface APIService {
    @GET("d97f27b5-caba-4cc8-9f5d-32b0208ec7f0")
    suspend fun fetchAccounts(): Accounts

    @GET("7a8a340d-b450-4adc-bbba-f6b4c8cdbc09")
    suspend fun fetchTransactions(): Transactions
}
