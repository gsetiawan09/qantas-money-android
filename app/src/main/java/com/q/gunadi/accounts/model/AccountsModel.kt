package com.q.gunadi.accounts.model

interface AccountsModel {
    suspend fun getAccounts(): List<Account>
}
