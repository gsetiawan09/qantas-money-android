package com.q.gunadi.accounts.presenter

import com.q.gunadi.BasePresenter
import com.q.gunadi.accounts.view.adapter.AccountListAdapter
import com.q.gunadi.accounts.model.AccountsModel
import com.q.gunadi.accounts.view.ui.AccountsView
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.lang.Exception

class AccountsPresenter(
    private val view: AccountsView
) : BasePresenter<AccountsView>(),
    AccountListAdapter.AccountActionListener, KoinComponent {

    private val accountsModel: AccountsModel by inject()

    fun onViewCreated() {
        view.accountListAdapter?.listener = this
        loadAccounts()
    }

    internal fun loadAccounts() {
        view.displayProgress()
        launch {
            try {
                val accounts = accountsModel.getAccounts()
                withContext(Dispatchers.Main) {
                    view.hideProgress()
                    view.displayAccounts(accounts)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    view.hideProgress()
                    view.displayError(e.toString())
                }
            }
        }
    }

    override fun onAccountClicked() {
        view.showTransactions()
    }
}
