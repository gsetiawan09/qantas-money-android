package com.q.gunadi.accounts.model

data class Accounts(
    val accounts: List<Account>
)

data class Account(
    val id: Number,
    val name: String,
    val balance: Number,
    val available_balance: Number,
    val currency: String
)
