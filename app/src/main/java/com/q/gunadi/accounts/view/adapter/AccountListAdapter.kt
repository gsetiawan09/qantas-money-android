package com.q.gunadi.accounts.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.q.gunadi.utils.OnSingleClickListener
import com.q.gunadi.R
import com.q.gunadi.accounts.model.Account
import com.q.gunadi.utils.getAmountText
import kotlinx.android.synthetic.main.view_item_account.view.*

class AccountListAdapter : RecyclerView.Adapter<AccountListAdapter.AccountsViewHolder>() {

    interface AccountActionListener {
        fun onAccountClicked()
    }

    var accounts : List<Account> = arrayListOf()

    var listener: AccountActionListener? = null

    class AccountsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(accountName: String,
                 balance: Number,
                 availableBalance: Number,
                 currency: String,
                 clickListener: View.OnClickListener) {

            itemView.description.text = accountName
            itemView.balance.text = itemView.context.getString(R.string.balance,
                getAmountText(currency, balance)
            )
            itemView.available_balance.text = itemView.context.getString(R.string.available_balance,
                getAmountText(currency, availableBalance)
            )
            itemView.setOnClickListener(clickListener)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int) =
        AccountsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_item_account, parent, false
            )
        )

    override fun onBindViewHolder(holder: AccountsViewHolder, position: Int) {
        accounts[position].let {
            holder.bind(it.name, it.balance, it.available_balance, it.currency,
                OnSingleClickListener { listener?.onAccountClicked() })
        }
    }

    override fun getItemCount() = accounts.size
}
