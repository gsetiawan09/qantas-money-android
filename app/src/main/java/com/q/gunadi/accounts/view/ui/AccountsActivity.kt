package com.q.gunadi.accounts.view.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.q.gunadi.BaseActivity
import com.q.gunadi.R
import com.q.gunadi.accounts.model.Account
import com.q.gunadi.accounts.view.adapter.AccountListAdapter
import com.q.gunadi.accounts.presenter.AccountsPresenter
import com.q.gunadi.databinding.ActivityAccountsBinding
import com.q.gunadi.transactions.view.ui.TransactionsActivity
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class AccountsActivity : BaseActivity<AccountsView, AccountsPresenter>(),
    AccountsView {

    override val presenter: AccountsPresenter by inject { parametersOf(this) }

    private lateinit var binding: ActivityAccountsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        supportActionBar?.apply {
            title = getString(R.string.accounts_title)
        }

        accountListAdapter =
            AccountListAdapter()
        binding.accountsList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@AccountsActivity)
            adapter = accountListAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        presenter.onViewCreated()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override var accountListAdapter: AccountListAdapter? = null

    override fun displayAccounts(accounts: List<Account>) {
        Timber.d("displayAccounts")
        accountListAdapter?.let {
            it.accounts = accounts
            it.notifyDataSetChanged()
        }
    }

    override fun displayProgress() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.progressBar.visibility = View.GONE
    }

    override fun showTransactions() {
        startActivity(TransactionsActivity.createIntent(this))
    }
}
