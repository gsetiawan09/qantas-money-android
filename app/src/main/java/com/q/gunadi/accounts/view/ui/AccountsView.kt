package com.q.gunadi.accounts.view.ui

import com.q.gunadi.BaseView
import com.q.gunadi.accounts.model.Account
import com.q.gunadi.accounts.presenter.AccountsPresenter
import com.q.gunadi.accounts.view.adapter.AccountListAdapter

interface AccountsView : BaseView<AccountsPresenter> {
    var accountListAdapter: AccountListAdapter?
    fun displayAccounts(accounts: List<Account>)
    fun showTransactions()
}
