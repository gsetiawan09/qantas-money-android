package com.q.gunadi.accounts.model

import com.q.gunadi.APIService
import org.koin.core.KoinComponent
import org.koin.core.inject

class AccountsModelImpl : AccountsModel, KoinComponent {

    private val apiService: APIService by inject()

    override suspend fun getAccounts() = apiService.fetchAccounts().accounts
}
