package com.q.gunadi

interface BaseView<T> {
    fun displayError(errorMessage: String? = null)
    fun displayProgress()
    fun hideProgress()
}
