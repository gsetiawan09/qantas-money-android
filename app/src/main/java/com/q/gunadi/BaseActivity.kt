package com.q.gunadi

import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder

abstract class BaseActivity<V : BaseView<P>, P : BasePresenter<V>> : AppCompatActivity(), BaseView<P> {

    abstract val presenter: P

    override fun displayError(errorMessage: String?) {
        MaterialAlertDialogBuilder(this)
            .setMessage(errorMessage ?: getString(R.string.generic_error_message))
            .setPositiveButton(R.string.ok_button, null)
            .show()
    }
}
