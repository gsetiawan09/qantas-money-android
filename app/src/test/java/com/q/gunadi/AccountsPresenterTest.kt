package com.q.gunadi

import com.nhaarman.mockitokotlin2.*
import com.q.gunadi.accounts.model.Account
import com.q.gunadi.accounts.model.AccountsModel
import com.q.gunadi.accounts.presenter.AccountsPresenter
import com.q.gunadi.accounts.view.ui.AccountsView
import com.q.gunadi.di.applicationModule
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.parameter.parametersOf
import org.koin.test.KoinTest
import org.koin.test.inject
import org.koin.test.mock.declareMock
import org.mockito.Mockito
import java.lang.RuntimeException

@ExperimentalCoroutinesApi
class AccountsPresenterTest : KoinTest {

    private val view: AccountsView = mock()
    private val model: AccountsModel by inject()
    private val presenter: AccountsPresenter by inject { parametersOf(view) }
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun before() {
        startKoin { modules(applicationModule) }
        declareMock<AccountsModel>()
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun after() {
        stopKoin()
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `verify that loadAccounts invokes getAccounts service call and update the view`() {
        val listOfAccounts = listOf(
            Account(0, "test account 1", 123.44, 122.00, "AUD")
        )
        runBlocking {
            whenever(model.getAccounts()).thenReturn(listOfAccounts)
        }
        doNothing().whenever(view).displayProgress()

        presenter.loadAccounts()
        runBlocking {
            Mockito.verify(model).getAccounts()

            withContext(Dispatchers.Main) {
                Mockito.verify(view).displayAccounts(listOfAccounts)
            }
        }
    }

    @Test
    fun `verify that loadAccounts invokes getAccounts service call and show error`() {
        runBlocking {
            whenever(model.getAccounts()).thenThrow(RuntimeException::class.java)
        }
        doNothing().whenever(view).displayProgress()

        presenter.loadAccounts()
        runBlocking {
            Mockito.verify(model).getAccounts()
            withContext(Dispatchers.Main) {
                Mockito.verify(view).displayError(any())
            }
        }
    }
}
