package com.q.gunadi

import com.nhaarman.mockitokotlin2.*
import com.q.gunadi.di.applicationModule
import com.q.gunadi.transactions.model.*
import com.q.gunadi.transactions.presenter.TransactionsPresenter
import com.q.gunadi.transactions.view.ui.TransactionsView
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.parameter.parametersOf
import org.koin.test.KoinTest
import org.koin.test.inject
import org.koin.test.mock.declareMock
import org.mockito.ArgumentMatchers.anyList
import org.mockito.Mockito
import java.lang.RuntimeException

@ExperimentalCoroutinesApi
class TransactionsPresenterTest : KoinTest {

    private val view: TransactionsView = mock()
    private val model: TransactionsModel by inject()
    private val presenter: TransactionsPresenter by inject { parametersOf(view) }
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun before() {
        startKoin { modules(applicationModule) }
        declareMock<TransactionsModel>()
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun after() {
        stopKoin()
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `verify that loadTransactions invokes getTransactions service call and update the view`() {
        val transaction1 = Transaction(
            0,
            "2019-02-01T12:35:51.257Z",
            "description",
            122.00,
            "AUD",
            merchant = Merchant("Waitrose & Partners", null, null),
            isPending = true
        )

        val list = listOf(
            DateItem("2019-02-01"), TransactionItem(transaction1)
        )
        runBlocking {
            whenever(model.getTransactions()).thenReturn(list)
        }
        doNothing().whenever(view).displayProgress()

        presenter.loadTransactions()
        runBlocking {
            Mockito.verify(model).getTransactions()

            withContext(Dispatchers.Main) {
                Mockito.verify(view).displayTransactions(anyList())
            }
        }
    }

    @Test
    fun `verify that loadTransactions invokes getTransactions service call and show error`() {
        runBlocking {
            whenever(model.getTransactions()).thenThrow(RuntimeException::class.java)
        }
        doNothing().whenever(view).displayProgress()

        presenter.loadTransactions()
        runBlocking {
            Mockito.verify(model).getTransactions()
            withContext(Dispatchers.Main) {
                Mockito.verify(view).displayError(any())
            }
        }
    }
}
