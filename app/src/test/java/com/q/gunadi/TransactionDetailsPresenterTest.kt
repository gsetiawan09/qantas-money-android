package com.q.gunadi

import com.nhaarman.mockitokotlin2.*
import com.q.gunadi.di.applicationModule
import com.q.gunadi.transactiondetails.presenter.TransactionDetailsPresenter
import com.q.gunadi.transactiondetails.view.TransactionDetailsView
import com.q.gunadi.transactions.model.Merchant
import com.q.gunadi.transactions.model.Transaction
import kotlinx.coroutines.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.parameter.parametersOf
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class TransactionDetailsPresenterTest : KoinTest {

    private val view: TransactionDetailsView = mock()
    private val presenter: TransactionDetailsPresenter by inject { parametersOf(view) }

    @Before
    fun before() {
        startKoin { modules(applicationModule) }
    }

    @After
    fun after() {
        stopKoin()
    }

    @Test
    fun `verify that loadTransactionDetails updates the view`() {
        val transaction1 = Transaction(
            0,
            "2019-02-01T12:35:51.257Z",
            "description",
            122.00,
            "AUD",
            merchant = Merchant("Waitrose & Partners", null, null),
            isPending = true
        )

        presenter.loadTransactionDetails(transaction1)

        Mockito.verify(view).displayTransactionDetails(any(), any(), any(), any(), any(), any(), anyOrNull())
    }

    @Test
    fun `verify that if loadTransactionDetails with null transaction then show error`() {
        presenter.loadTransactionDetails(null)

        Mockito.verify(view).displayError()
    }
}
